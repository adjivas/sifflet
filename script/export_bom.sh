#!/bin/sh

# Export a bom XML
#
# Using UI automation to export KiCad schematics -
# https://scottbezek.blogspot.com/2016/04/automated-kicad-schematic-export.html
#
# ```
# Xvfb $DISPLAY -screen 0 1024x768x16
# x11vnc -display $DISPLAY -bg -forever -nopw -quiet -listen localhost -xkb
# vncviewer localhost:0
# xwd -root -silent | convert xwd:- png:/tmp/sh0.png
# ```

source=$1
eeschema $source &
eeschema_proc=$!

sleep 1
# Configurer la Table Globale des Librairies Symbole
xdotool search --onlyvisible --class eeschema windowfocus
# Valider
xdotool key Return

sleep 1
# Eeschema
xdotool search --onlyvisible --class eeschema windowfocus
# Eeschema, Outils -> Générer Liste du Matériel...
xdotool key Return alt+o m

sleep 1
# Liste du Matériel, Générer
xdotool key Return

sleep 1
# Eeschema, Quitter
kill -9 $eeschema_proc
