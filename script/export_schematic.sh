#!/bin/sh

# Export a schematic PDF
#
# Using UI automation to export KiCad schematics -
# https://scottbezek.blogspot.com/2016/04/automated-kicad-schematic-export.html
#
# ```
# Xvfb $DISPLAY -screen 0 1024x768x16
# x11vnc -display $DISPLAY -bg -forever -nopw -quiet -listen localhost -xkb
# vncviewer localhost:0
# xwd -root -silent | convert xwd:- png:/tmp/sh0.png
# ```

source=$1

eeschema $source &
eeschema_proc=$!

sleep 1
# Configurer la Table Globale des Librairies Symbole
xdotool search --onlyvisible --class eeschema windowfocus
# Valider
xdotool key Return

sleep 1
# Eeschema
xdotool search --onlyvisible --class eeschema windowfocus
# Fichiers -> Tracer
xdotool key Return alt+f t

sleep 1
# Options de Tracé Schématique
xdotool search --onlyvisible --class Plot windowfocus
# Répertoire de sortie: /tmp
xdotool type "/tmp"

sleep 1
# SVG, Tracer Toutes les Pages
xdotool mousemove 100 290 click 1
xdotool key Return

sleep 1
# PDF, Tracer Toutes les Pages
xdotool mousemove 100 265 click 1
xdotool key Return

sleep 1
# Fermer
xdotool key Escape

# Eeschema
xdotool search --onlyvisible --class eeschema windowfocus
# Quitter
xdotool key ctrl+q
